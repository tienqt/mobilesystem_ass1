package com.example.tien.assignment1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

// This class display location history. lat, lng and date is provided by an showMapIntent
public class showHistory extends Activity {

    private Intent intent;
    private Bundle exstra;
    private Intent showMapIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_history);

        showMapIntent = new Intent(this, showMap.class);

        intent = getIntent();
        ListView historyList = (ListView) findViewById(R.id.historyList);
        exstra = new Bundle();
        exstra = intent.getExtras();
        final String lat[] = exstra.getStringArray("lat");
        final String date[] = exstra.getStringArray("date");
        final String lng[] = exstra.getStringArray("lng");

        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

        for(int i = 0; i<lng.length; i++) {
            HashMap<String, String> noe = new HashMap<String, String>();
            String mainLocation = "Lat " + lat[i] + "\nLng "+ lng[i];
            noe.put("line1", mainLocation);
            noe.put("line2", date[i]);

            list.add(noe);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter
                  (this, list, android.R.layout.two_line_list_item, new String[] {"line1", "line2"},
                  new int[] {android.R.id.text1, android.R.id.text2});

        historyList.setAdapter(simpleAdapter);


       historyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               Bundle extra = new Bundle();
               extra.putDouble("lat", Double.valueOf(lat[i]));
               extra.putDouble("lng", Double.valueOf(lng[i]));
               showMapIntent.putExtras(extra);
               startActivity(showMapIntent);
           }
       });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.show_history, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
