package com.example.tien.assignment1;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tien on 14.09.2014.
 */

// This class stores location information. It also include methods for writing and reading from database.
// most of the is taken from https://bitbucket.org/gtl-hig/imt3662_sql_notes/src/486e19d09acd78b088627bececc3e013bbbfb8cf/src/no/hig/imt3662/notes/Note.java?at=master

public class LocationHistory {

        long id;
        String lat;
        String lng;
        String date;

        public LocationHistory() {}

        public LocationHistory(final String title, final String body, final String date) {
            this.lat = title;
            this.lng = body;
            this.date = date;
        }

        public void save(DatabaseHelper dbHelper) {
            final ContentValues values = new ContentValues();
            values.put(LAT, this.lat);
            values.put(LNG, this.lng);
            values.put(DATE, this.date);

            final SQLiteDatabase db = dbHelper.getReadableDatabase();
            this.id = db.insert(LOCATIONS_TABLE_NAME, null, values);
            db.close();
        }

        public static LocationHistory[] getAll(final DatabaseHelper dbHelper) {
            final List<LocationHistory> locations = new ArrayList<LocationHistory>();
            final SQLiteDatabase db = dbHelper.getWritableDatabase();
            final Cursor c = db.query(LOCATIONS_TABLE_NAME,
                    new String[] { ID, LAT, LNG, DATE}, null, null, null, null, null);
            // make sure you start from the first item
            c.moveToFirst();
            while (!c.isAfterLast()) {
                final LocationHistory location = cursorToNote(c);
                locations.add(location);
                c.moveToNext();
            }
            // Make sure to close the cursor
            c.close();
            return locations.toArray(new LocationHistory[locations.size()]);
        }

        public static LocationHistory cursorToNote(Cursor c) {
            final LocationHistory location = new LocationHistory();
            location.setLat(c.getString(c.getColumnIndex(LAT)));
            location.setLng(c.getString(c.getColumnIndex(LNG)));
            location.setDate(c.getString(c.getColumnIndex(DATE)));
            return location;
        }


        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String toString() {
            return this.lat;
        }



        public static final String LOCATIONS_TABLE_NAME = "locations";
        // column names
        static final String ID = "id"; //
        static final String LAT = "latitude";
        static final String LNG = "longitude";
        static final String DATE = "date";
        // SQL statement to create our table
        public static final String LOCATIONS__CREATE_TABLE = "CREATE TABLE " + LocationHistory.LOCATIONS_TABLE_NAME + " ("
                + LocationHistory.ID + " INTEGER PRIMARY KEY,"
                + LocationHistory.LAT + " TEXT,"
                + LocationHistory.LNG + " TEXT,"
                + LocationHistory.DATE + " TEXT"
                + ");";


}
