package com.example.tien.assignment1;

import android.os.AsyncTask;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Tien on 13.09.2014.
 */
// this class manages opening a connection, downloading xml and parsing it.
// Most of the code is taken from
// https://bitbucket.org/gtl-hig/imt3662_file_download/src/e600d3d18f2a2cb7343b2abd3b081f232c009fbc/src/no/hig/imt3662/filedownload/MainActivity.java?at=master

public class YrParser {

    private DownloadRSSTask yrData;
    private URL link;

    public YrParser(URL link, TextView txt) {
        yrData = new DownloadRSSTask(txt);
        this.link = link;
    }

    public void start() {

        yrData.execute(link);
    }

    private class DownloadRSSTask extends AsyncTask<URL, Void, String[]> {
        private TextView textView;

        public DownloadRSSTask(TextView txt){
            textView = txt;
        }

        @Override
        protected String[] doInBackground(URL... urls) {
            assert urls.length == 1;
            return downloadRSS(urls[0]);
        }

        @Override
        protected void onPostExecute(String[] results) {
            String res = "The weather at your current location is " + results[0] + " C";
            textView.setText(res);
        }
    }

    private String[] downloadRSS(final URL url) {
        InputStream in = null;
        String[] RSSTitles = new String[1];
        try {
            in = openHttpConnection(url);
            Document doc = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;

            try {
                db = dbf.newDocumentBuilder();
                doc = db.parse(in);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }

            doc.getDocumentElement().normalize();

            //---retrieve all the <item> nodes---
            Node forecast = doc.getElementsByTagName("forecast").item(0);
            Node tabular = ((Element) forecast).getElementsByTagName("tabular").item(0);
            Node time = ((Element) tabular).getElementsByTagName("time").item(0);
            Node temp = ((Element) time).getElementsByTagName("temperature").item(0);
            RSSTitles[0] = ((Element) temp).getAttribute("value");

        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return RSSTitles;
    }

    private InputStream openHttpConnection(final URL url) throws IOException {
        InputStream in = null;
        int response = -1;
        final URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }

        try {
            final HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return in;
    }

}
