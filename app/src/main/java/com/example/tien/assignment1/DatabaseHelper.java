package com.example.tien.assignment1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Tien on 15.09.2014.
 */

// This class manager the database.
// This code is taken from https://bitbucket.org/gtl-hig/imt3662_sql_notes/src/486e19d09acd78b088627bececc3e013bbbfb8cf/src/no/hig/imt3662/notes/DatabaseHelper.java?at=master
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "LocationsHis.db";
    public static final int DATABASE_VERSION = 1;


    /**
     * Sets up db helper.
     * @param ctx activity context
     */
    public DatabaseHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LocationHistory.LOCATIONS__CREATE_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO How did our DB change? Have we added new column? Renamed the column?
        // Now - handle the change.
    }

}
