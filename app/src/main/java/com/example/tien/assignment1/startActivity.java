package com.example.tien.assignment1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;

// Main class. Thiss class starts all the core funtions.
public class startActivity extends Activity {

    private Button showMapBut;                  // Start google maps
    private Button getWeatherBut;               // Get weather data from yr.no
    private Button getLocBut;                   // Get current location from GPS
    private Button showHistoryBut;              // Show previous locations
    private Button saveBut;                     // Saves data into database

    private TextView myLocationTxt;             // Show location data
    private TextView weatherTxt;                // Show weather in celsius
    private TextView savedTxt;                   // Display saved message

    private Intent showHistoryIntent;           // Start and pass along data to showHistory
    private Intent showLocationMapIntent;       // Start and pass along data to
                                                // showLocationMapIntent

    private LocationManager locationManager;    // Used to get the users location
    private Geocoder getPosFromLatLng;          // Used to retrieve address from latLng
    private Address userAddress;                // Stores the user's address information

    private LatLng locationLatLng;              // Users current lat and lng

    private YrParser yrData;                    // Download and pars xml from yr.no

    private DatabaseHelper databaseHelper;

                                                // link to xml file from yr.no
    private final String firstUrl = "http://www.yr.no/sted/Norge/postnummer/";
    private final String endUrl = "/varsel.xml";

    private long timeStamp;
    private SimpleDateFormat dateFormat;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        // Initialing UI components from activity_main.xml
        showMapBut = (Button) findViewById(R.id.showMapBut);
        getWeatherBut = (Button) findViewById(R.id.getWeatherBut);
        getLocBut = (Button) findViewById(R.id.getLocBut);
        showHistoryBut = (Button) findViewById(R.id.historyBut);
        saveBut = (Button) findViewById(R.id.saveBut);

        myLocationTxt = (TextView) findViewById(R.id.myPosTxt);
        weatherTxt = (TextView) findViewById(R.id.showWeatherTxt);
        savedTxt = (TextView) findViewById(R.id.okTxt);

        getPosFromLatLng = new Geocoder(this, Locale.ENGLISH);
        databaseHelper = new DatabaseHelper(this);
        dateFormat = new SimpleDateFormat("yyyy.MM.dd\tH:mm:ss",Locale.ENGLISH);

        showHistoryIntent = new Intent(this, showHistory.class);
        showLocationMapIntent = new Intent(this, showMap.class);

        showMapBut.setEnabled(false);           // Is disabled before getLocation is called
        getWeatherBut.setEnabled(false);        // Same as d(^^^^^^^___^^^^^^)b
        saveBut.setEnabled(false);



        // OnClick listener for all five buttons
        // (1)showMapBut (2)getWeatherBut (3)getLocBut  (4)showHistoryBut (5)saveBut
        showMapBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showMap();
            }
        });
        getWeatherBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getWeather();
            }
        });
        getLocBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getLocation();
            }
        });
        showHistoryBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showHistory();
            }
        });
        saveBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                saveHistory();
            }
        });


        // Loads in last position, if any
        LocationHistory test[] = LocationHistory.getAll(databaseHelper);
        if(test.length != 0) {
            try {
                locationLatLng = new LatLng(Double.valueOf(test[test.length-1].getLat()), Double.valueOf(test[test.length-1].getLng()));
                userAddress = getPosFromLatLng.getFromLocation
                        (Double.valueOf(test[test.length-1].getLat()), Double.valueOf(test[test.length-1].getLng()), 1).get(0);

                String location = "Your current address is "
                        + userAddress.getAddressLine(0) + ", "
                        + userAddress.getAddressLine(1) + ", "
                        + userAddress.getAddressLine(2)
                        + "\n\nCurrent coordinates\n"
                        + "Latitude " + locationLatLng.latitude + "\n"
                        + "Longitude " + locationLatLng.longitude;

                myLocationTxt.setText(location);
                getWeatherBut.setEnabled(true);
                showMapBut.setEnabled(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    // Starts a new google maps activity and sets the camera and marker
    // to the users current latitude and longitude
    private void showMap() {

        Bundle extra = new Bundle();
        extra.putDouble("lat", locationLatLng.latitude);
        extra.putDouble("lng", locationLatLng.longitude);
        showLocationMapIntent.putExtras(extra);
        startActivity(showLocationMapIntent);
    }

    // Gets weather information for the users location from yr.no
    private void getWeather() {

        final String finalUrl = firstUrl + userAddress.getPostalCode() + endUrl;
        try {
            URL yrURL = new URL(finalUrl);
            yrData = new YrParser(yrURL, weatherTxt);
            yrData.start();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    // Gets the users current location from GPS
    private void getLocation() {

        myLocationTxt.setText("Getting current location. \nThis may take up to a few minutes depending on your location.");
        getLocBut.setEnabled(false);
        weatherTxt.setText("");

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                String showLocation;
                locationLatLng = new LatLng(location.getLatitude(), location.getLongitude());

                try {
                    userAddress = getPosFromLatLng.getFromLocation(locationLatLng.latitude, locationLatLng.longitude, 1).get(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                showLocation = "Your current address is "
                                  + userAddress.getAddressLine(0) + ", "
                                  + userAddress.getAddressLine(1) + ", "
                                  + userAddress.getAddressLine(2)
                                  + "\n\nCurrent coordinates\n"
                                  + "Latitude " + locationLatLng.latitude + "\n"
                                  + "Longitude " + locationLatLng.longitude;

                timeStamp = location.getTime();
                savedTxt.setText("");

                myLocationTxt.setText(showLocation);
                getLocBut.setEnabled(true);
                showMapBut.setEnabled(true);
                getWeatherBut.setEnabled(true);
                saveBut.setEnabled(true);

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        });

        showMapBut.setEnabled(false);
        getWeatherBut.setEnabled(false);
    }

    // Creates a new activity and show the users past locations
    // date lat lon
    private void showHistory() {
        Bundle exstras = new Bundle();
        String lng[];
        String lat[];
        String date[];
        LocationHistory[] locations = LocationHistory.getAll(databaseHelper);
        lat = new String[locations.length];
        date = new String[locations.length];
        lng = new String[locations.length];

        for(int i = 0; i<locations.length; i++) {
            date[i] = locations[i].getDate();
            lng[i] = locations[i].getLng();
            lat[i] = locations[i].getLat();
        }
        exstras.putStringArray("lng", lng);
        exstras.putStringArray("lat", lat);
        exstras.putStringArray("date", date);

        showHistoryIntent.putExtras(exstras);
        startActivity(showHistoryIntent);
    }

    private void saveHistory() {
        LocationHistory loc = new LocationHistory
                (String.valueOf(locationLatLng.latitude), String.valueOf(locationLatLng.longitude),
                 String.valueOf(dateFormat.format(timeStamp)));

        loc.save(databaseHelper);
        savedTxt.setText("Saved!");
        saveBut.setEnabled(false);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }





}
